const getAllCourseMiddleware = (request, response, next) => {
    console.log("Get ALL Course Middleware");
    next();
}

const createCourseMiddleware = (request, response, next) => {
    console.log("Create Course Middleware");
    next();
}

const getDetailCourseMiddleware = (request, response, next) => {
    console.log("Get Detail Course Middleware");
    next();
}

const updateCourseMiddleware = (request, response, next) => {
    console.log("Update Course Middleware");
    next();
}

const deleteCourseMiddleware = (request, response, next) => {
    console.log("Delete Course Middleware");
    next();
}

module.exports = {
    getAllCourseMiddleware,
    createCourseMiddleware,
    getDetailCourseMiddleware,
    updateCourseMiddleware,
    deleteCourseMiddleware
}
